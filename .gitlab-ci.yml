image: node:lts-alpine

.releases:
  rules:
    - if: $CI_COMMIT_BRANCH =~ /^(master|beta)/ && $CI_PIPELINE_SOURCE != 'schedule' && $CI_COMMIT_MESSAGE =~ /^(feat|fix).*/

workflow:
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /^chore\(release\).*/ || $CI_COMMIT_TAG
      when: never
    - when: always

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .pnpm-store

before_script:
  - apk update --quiet
  - apk add --quiet curl git ack
  - curl -LSs https://unpkg.com/@pnpm/self-installer | node
  - pnpm config set store-dir .pnpm-store
  - pnpm set verify-store-integrity false
  - pnpm install --reporter=silent

stages:
  - sync
  - test
  - build
  - release

variables:
  PACKAGE_REGISTRY_URL: $CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/generic/module

sync:
  stage: sync
  script:
    - eval $(ssh-agent -s)
    - echo "$DEPLOY_KEY" | tr -d '\r' | ssh-add -
    - curl -L --output pf2e.zip https://github.com/foundryvtt/pf2e/releases/download/latest/pf2e.zip
    - unzip -q pf2e.zip lang/en.json
    - unzip -q pf2e.zip lang/re-en.json
    - xargs -I % -a .packs unzip -q pf2e.zip packs/%.db
    - pnpm --silent exec fvttp --path packs --babele --use-name --no-items --save packs
    - mv lang/en.json pf2e_pt-BR/lang/en
    - mv lang/re-en.json pf2e_pt-BR/lang/en
    - mv packs/*.json pf2e_pt-BR/lang/en
    - pnpm --silent exec jsonlint -q --enforce-double-quotes pf2e_pt-BR
    - git add pf2e_pt-BR/lang
    - "git commit -m 'chore(sync): update files' || echo 'No changes, nothing to commit!'"
    - git remote rm origin && git remote add origin git@gitlab.com:$CI_PROJECT_PATH.git
    - git push origin HEAD:$CI_COMMIT_REF_NAME
  rules:
    - if: $CI_PIPELINE_SOURCE == 'schedule'

jsonlint:
  stage: test
  script:
    - pnpm --silent exec jsonlint -q --enforce-double-quotes pf2e_pt-BR
  rules:
    - if: $CI_PIPELINE_SOURCE != 'schedule'
      changes:
        - pf2e_pt-BR/**/*.json

semistandard:
  stage: test
  script:
    - pnpm --silent exec semistandard
  rules:
    - if: $CI_PIPELINE_SOURCE != 'schedule'
      changes:
        - pf2e_pt-BR/**/*.js

build:
  stage: build
  script:
    - export VERSION=$(pnpm --silent exec semantic-release --dry-run | ack -io '(?<=The next release version is )(.*)(?=$)') && echo VERSION=$VERSION >> build.env
    - export MANIFEST=$CI_PROJECT_URL/-/raw/$CI_COMMIT_BRANCH/pf2e_pt-BR/module.json && echo MANIFEST=$MANIFEST >> build.env
    - export DOWNLOAD=$CI_PROJECT_URL/-/releases/v$VERSION/downloads/module.zip && echo DOWNLOAD=$DOWNLOAD >> build.env
    - pnpm run build
  artifacts:
    name: module
    when: on_success
    reports:
      dotenv: build.env
    paths:
      - package.json
      - module.zip
      - pf2e_pt-BR/module.json
  extends: .releases

publish:
  stage: release
  script:
    - pnpm --silent exec semantic-release
  extends: .releases
