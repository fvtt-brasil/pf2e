# FoundryVTT Pathfinder 2e Brazilian Portuguese

## Português

Esse módulo adiciona o idioma Português (Brasil) como uma opção a ser selecionada nas configurações do FoundryVTT. Selecionar essa opção traduzirá a ficha de personagem do sistema PF2E e mais alguns aspectos menores. No momento, os compêndios não estão traduzidos.
Esse módulo traduz somente aspectos relacionados ao sistema de [Pathfinder 2E](https://gitlab.com/hooking/foundry-vtt---pathfinder-2e/). Esse módulo não traduz outras partes do software FoundryVTT, como a interface principal. Para isso, confira o módulo [Brazillian Portuguese Core](https://foundryvtt.com/packages/ptBR-core/).

Para a tradução dos Compêndios, é necessário que seja instalado o módulo [Babele](https://foundryvtt.com/packages/babele/).

Se encontrar algum erro na tradução ou tiver uma sugestão de algum termo melhor para ser usado em alguma parte dela você pode abrir uma nova issue ou enviar uma mensagem para **DMerceless#1356** no Discord.

> Atualizado para funcionar com a versão 0.7.0 do FoundryVTT e com a versão 1.10.5 do sistema Pathfinder Second Edition.


### Instalação
A tradução está disponível na lista de Módulos Complementares para instalar com o nome de Translation: Brazilian Portuguese [Pathfinder 2e].

Lembre-se de também instalar o módulo [Babele](https://foundryvtt.com/packages/babele/) para a tradução dos Compêndios.

### Instalação por Manifesto
Na opção Add-On Modules clique em Install Module e coloque o seguinte link no campo Manifest URL

`https://gitlab.com/foundryvtt-pt-br/pathfinder-2e-pt-br/-/raw/master/pf2e_pt-BR/module.json`

### Instalação Manual
Se as opções acima não funcionarem, faça o download do arquivo [pathfinder-2e-pt-br-master-pf2e_pt-BR.zip](https://gitlab.com/foundryvtt-pt-br/pathfinder-2e-pt-br/-/archive/master/pathfinder-2e-pt-br-master.zip?path=pf2e_pt-BR) e extraia a pasta pf2e_pt-BR dentro da pasta Data/modules/
Feito isso ative o módulo nas configurações do mundo em que pretende usá-lo e depois altere o idioma nas configurações.


## English
This module adds the language Português (Brasil) as an option to be selected in the FoundryVTT settings. Selecting this option will translate the character sheets of the PF2E system and some other minor aspects. Currently, the compendium content is not translated.
This module translates only aspects related to the [Pathfinder 2E system](https://gitlab.com/hooking/foundry-vtt---pathfinder-2e/). This module doesn't translate other parts of the FoundryVTT software, like the main interface. For that, take a look at the [Brazillian Portuguese Core](https://foundryvtt.com/packages/ptBR-core/) module.

For translating the Compendium packs, it is necessary to also install the [Babele](https://foundryvtt.com/packages/babele/) module.

If you find a mistake in the translation or have a suggestion of a better term to use somewhere in it you can open a new issue or send a message to **DMerceless#1356** at Discord.

> Updated to work with version 0.7.0 of FoundryVTT and with version 1.10.5 of the Pathfinder Second Edition System.


### Installation
The translation is available in the Add-on Modules list to install with the name Translation: Brazilian Portuguese [Pathfinder 2e].

Remember to also install the [Babele](https://foundryvtt.com/packages/babele/) module for Compendium pack translation.

### Manifest Installation
In the Add-On Modules option click on Install Module and place the following link in the field Manifest URL

`https://gitlab.com/foundryvtt-pt-br/pathfinder-2e-pt-br/-/raw/master/pf2e_pt-BR/module.json`

### Manual Installation
If the above options do not work, download the [pathfinder-2e-pt-br-master-pf2e_pt-BR.zip](https://gitlab.com/foundryvtt-pt-br/pathfinder-2e-pt-br/-/archive/master/pathfinder-2e-pt-br-master.zip?path=pf2e_pt-BR) file and extract the pf2e_pt-BR folder into the Data/modules/ folder
Once this is done, enable the module in the settings of the world in which you intend to use it and then change the language in the settings.
